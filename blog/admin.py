from django.contrib import admin
from .models import author, article, category
# Register your models here.


class authorModel(admin.ModelAdmin):
    list_display = ["__str__"]
    search_fields = ["__str__", "details"]

    class Meta:
        Model = author


admin.site.register(author, authorModel)

# Author End


class articleModel(admin.ModelAdmin):
    list_display = ["__str__", "postedOn", "updatedOn", "article_author"]
    search_fields = ["__str__", "details"]
    list_per_page = 10
    list_filter = ["category", "postedOn"]

    class Meta:
        Model = article


admin.site.register(article, articleModel)

# Article End


class categoryModel(admin.ModelAdmin):
    list_display = ["__str__"]
    search_fields = ["__str__"]
    list_per_page = 10

    class Meta:
        Model = category


admin.site.register(category, categoryModel)

# Category End